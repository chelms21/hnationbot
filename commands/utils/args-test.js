module.exports = {
	name: 'args-test',
	description: 'args test',
	execute(message, args) {
	  message.react('👍');
		if (!args.length) {
      return message.reply('You have not provided any arguments!');
    } else if (args[0] === 'foo') {
      return message.reply('bar');
    }

    message.channel.send(`Args: ${args}`);
    message.channel.send(`Args Length: ${args.length}`);
	},
};